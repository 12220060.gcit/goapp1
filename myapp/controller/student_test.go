package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// test add student
func TestAddStudent(t *testing.T) {
	url := "http://localhost:8080/student"
	var data = []byte(`{"stdid":112, "fname":"namgay", "lname":"zam", "email":"nz@gmail.com"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.JSONEq(t, `{"message":"std data added"}`, string(body))

}

// test get student
func TestGetStudent(t *testing.T) {
	c := http.Client{}
	r, _ := c.Get("http://localhost:8080/student/112")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	resp := `{"stdid":112, "fname":"namgay", "lname":"zam","email":"nz@gmail.com"}`
	assert.JSONEq(t, resp, string(body))

}

// delete student
func TestDeleteStudent(t *testing.T) {
	url := "http://localhost:8080/student/1002"
	req, _ := http.NewRequest("DELETE", url, nil)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))
}

// test std not found
func TestStudentNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8080/student/10002")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Student not found"}`
	assert.JSONEq(expResp, string(body))
}
