CREATE DATABASE my_C_db;

CREATE TABLE course (
CourseID int NOT NULL,
CourseName varchar(45) NOT NULL,
PRIMARY KEY (CourseID),
UNIQUE (CourseName)
)
