package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdmLogin(t *testing.T) {
	url := "http://localhost:8080/login"
	var data = []byte(`{"email": "sanzin@gmail.com", "password": "pass"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data)) //simply creates request
	req.Header.Set("Content-Type", "application/json")
	//create client
	client := &http.Client{} //creating client
	//send post request
	resp, err := client.Do(req) //send request.
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close() //it will skip. its execute it when until the function terminates
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.JSONEq(t, `{"message":"success"}`, string(body))
}

func TestAdmUserNotExist(t *testing.T) {
	url := "http://localhost:8080/login"
	var data = []byte(`{"email": "san@gmail.com", "password": "pass"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data)) //simply creates request
	req.Header.Set("Content-Type", "application/json")
	//create client
	client := &http.Client{} //creating client
	//send post request
	resp, err := client.Do(req) //send request.
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close() //it will skip. its execute it when until the function terminates
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.JSONEq(t, `{"error":"sql: no rows in result set"}`, string(body))
}
