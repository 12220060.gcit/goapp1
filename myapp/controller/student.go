package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// STUDENT
func AddStudent(w http.ResponseWriter, r *http.Request) {
	//validate cookie
	if !VerifyCookie(w, r) {
		return
	}

	// fmt.Fprintf(w, "from add student handler")
	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		// w.Write([]byte(err.Error()))
		res_map := map[string]string{"error": "invalid json data"}
		response, _ := json.Marshal(res_map)
		w.Write(response)
		w.Header().Set("content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest) //400

		return
	}

	dbErr := stud.Create()
	if dbErr != nil {
		// w.Write([]byte(dbErr.Error()))
		res_map := map[string]string{"error": dbErr.Error()}
		response, _ := json.Marshal(res_map)
		w.Write(response)
		w.Header().Set("content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest) //400
		return
	}
	res_map := map[string]string{"message": "std data added"}
	response, _ := json.Marshal(res_map)
	w.Write(response)
	w.Header().Set("content-Type", "application/json")
	w.WriteHeader(http.StatusCreated) //201

	// w.Write([]byte("student data added"))
	// fmt.Println(stud)
}

// read
func GetStud(w http.ResponseWriter, r *http.Request) {
	//get url parameter
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)

	// stud := model.Student{StdID: stdId}
	// fmt.Println(stud)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdID: stdId}
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, s) //statuscreated
}

// convert string sid to int
func getUserId(userIdParam string) (int64, error) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}

// update
func UpdateStud(w http.ResponseWriter, r *http.Request) {
	//validate cookie
	if !VerifyCookie(w, r) {
		return
	}
	old_sid := mux.Vars(r)["sid"] //get from URL
	old_stdId, _ := getUserId(old_sid)

	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	updateErr := stud.Update(old_stdId)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		httpResp.ResponseWithJson(w, http.StatusOK, stud)

	}
}

// // delete
func DeleteStud(w http.ResponseWriter, r *http.Request) {
	//validate cookie
	if !VerifyCookie(w, r) {
		return
	}
	sid := mux.Vars(r)["sid"]
	StdID, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdID: StdID}
	if err := s.Delete(); err != nil {
		httpResp.ResponseWithJson(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

// get all students
func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	student, getErr := model.GetAllStuds()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, student)
}

// COURSE
func AddCourse(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "from add student handler")
	var cour model.Course
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cour)
	if err != nil {
		// w.Write([]byte(err.Error()))
		res_map := map[string]string{"error": "invalid json data"}
		response, _ := json.Marshal(res_map)
		w.Write(response)
		w.Header().Set("content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest) //400

		return
	}

	dbErr := cour.Create()
	if dbErr != nil {
		// w.Write([]byte(dbErr.Error()))
		res_map := map[string]string{"error": dbErr.Error()}
		response, _ := json.Marshal(res_map)
		w.Write(response)
		w.Header().Set("content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest) //400
		return
	}
	res_map := map[string]string{"message": "course data added"}
	response, _ := json.Marshal(res_map)
	w.Write(response)
	w.Header().Set("content-Type", "application/json")
	w.WriteHeader(http.StatusCreated) //201

	// w.Write([]byte("student data added"))
	// fmt.Println(stud)
}

// read
func GetCour(w http.ResponseWriter, r *http.Request) {
	//get url parameter
	cid := mux.Vars(r)["cid"]
	courseid, idErr := getCourseId(cid)

	// stud := model.Student{StdID: stdId}
	// fmt.Println(stud)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := model.Course{CourseID: courseid}
	getErr := c.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, c) //statuscreated
}

// convert string sid to int
func getCourseId(courseidParam string) (string, error) {
	return courseidParam, nil
}

func getCourId(courseidParam string) (string, error) {
	return courseidParam, nil
}

// update
func UpdateCour(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"] //get from URL
	old_courseId, _ := getCourId(old_cid)

	var cour model.Course
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cour)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	updateErr := cour.Update(old_courseId)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		httpResp.ResponseWithJson(w, http.StatusOK, cour)

	}
}

// // delete
func DeleteCour(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	CourseID, idErr := getCourseId(cid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := model.Course{CourseID: CourseID}
	if err := c.Delete(); err != nil {
		httpResp.ResponseWithJson(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

// get all courses
func GetAllCours(w http.ResponseWriter, r *http.Request) {
	course, getErr := model.GetAllCours()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, course)
}
