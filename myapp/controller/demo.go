package controller

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// homeHandlerFunc
func HomeHandler(w http.ResponseWriter, r *http.Request) { // resp interface .// request that sent from client to server
	_, err := w.Write([]byte("hello wrold")) // _= byte interger
	if err != nil {
		fmt.Println("error:", err)
	}
}

func ParameterHandler(w http.ResponseWriter, r *http.Request) {
	// these two arguemnt is fixed
	para := mux.Vars(r) //p is type of
	fmt.Println(para)

	name := para["myname"]
	fmt.Println(name)

	w.Write([]byte("my name is " + name))

	// if err != nil{
	// 	os.Exit(1)
	// }
}
