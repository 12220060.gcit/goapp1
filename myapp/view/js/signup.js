function signUp(){
    var _data ={
        firstname: document.getElementById("fname").value,
        lastname: document.getElementById("lname").value,
        email: document.getElementById("email").value,
        password: document.getElementById("pw1").value,
        pw: document.getElementById("pw2").value,
    }
    // //check whether paaword matches the confirm password
  
    if (_data.password !==_data.pw){
        alert("PASSWORD doesn't match!")
        return
    }
    //post because adding data
    fetch('/signup', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: {"Content-type" : " application.json; charset=UTF-8"}
    })
    .then(response => {
        if (response.status == 201){  //201 is success created
            window.open("index.html", "_self")
        }
    });
  }
  