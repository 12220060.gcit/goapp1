window.onload = function(){
    fetch('/courses')
    .then(response => response.text())
    .then(data => showCourses(data))
  }
  


function addCourse() {
    var data = getData()
      // courseid: parseInt(document.getElementById("cid").value),
      // coursename: document.getElementById("coursename").value,
    //   lname: document.getElementById("lname").value,
    //   email: document.getElementById("email").value,
    // };

    var cid = data.courseid;

 
     if(data.coursename == ""){
      alert("course name cannot be empty")
      return
      }
    fetch("/course", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-type": "application/json;charset-UTF-8" },
    }).then((response1) => {
      if (response1.ok) {
        fetch("/course/" + cid)
          .then((response2) => response2.text())
          .then((data) => showCourse(data));
      }else{
        throw new Error(response1.statusText)
      }
    }).catch(e =>{
        alert(e)
    })
    resetform();
  }

//to show one student
function showCourse(data){
    const course = JSON.parse(data)
    course.forEach(cour =>{
      newRow(cour)
    });
// Find a <table> element with id="myTable":
    var table = document.getElementById("myTable")
    // Create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length)
    // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
    var td=[]
    for (i =0;i<table.rows[0].cells.length;i++){
      td[i] = row.insertCell(i)
    }
     // Add student detail to the new cells:
    //  td[0].innerHTML=course.courseid
    //  td[1].innerHTML=course.coursename
    //  td[2].innerHTML='<input type = "button" onclick = "deleteCourse(this)" value="delete" id ="button-1">'
    //  td[3].innerHTML='<input type = "button" onclick = "updateCourse(this)" value="edit" id ="button-2">'
  

}

//set form fields to empty
function resetform(){
  document.getElementById("cid").value = "";
  document.getElementById("coursename").value = "";
}


//function to show all students

function showCourses(data){
    const courses = JSON.parse(data)
    courses.forEach(cour =>{
      // Find a <table> element with id="myTable":
      var table = document.getElementById("myTable")
      // Create an empty <tr> element and add to the last position of the table:
      var row = table.insertRow(table.length)
      // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
      var td=[]
      for (i =0;i<table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i)
      }
      // Add student detail to the new cells:
      td[0].innerHTML=cour.courseid
      td[1].innerHTML=cour.coursename
      td[2].innerHTML='<input type = "button" onclick = "deleteCourse(this)" value="delete" id ="button-1">'
      td[3].innerHTML='<input type = "button" onclick = "updateCourse(this)" value="edit" id ="button-2">'
  
    })
  }

function newRow(course){//create the new row for the table we have in the table
    var table = document.getElementById("myTable")
    // Create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length)
    // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
    var td=[]
    for (i =0;i<table.rows[0].cells.length;i++){
      td[i] = row.insertCell(i)
    }
    // Add student detail to the new cells:
    td[0].innerHTML=course.courseid
    td[1].innerHTML=course.coursename
  //   td[2].innerHTML=stud.lname
  //   td[3].innerHTML=stud.email
    td[2].innerHTML='<input type = "button" onclick = "deleteCourse(this)" value="delete" id ="button-1">'
    td[3].innerHTML='<input type = "button" onclick = "updateCourse(this)" value="edit" id ="button-2">'

  }
  
  var selectedRow= null

  function updateCourse(r){
    selectedRow = r.parentElement.parentElement;
    //fill in the form fields with selected row data
    document.getElementById("cid").value=selectedRow.cells[0].innerHTML//access the value store
    document.getElementById("coursename").value=selectedRow.cells[1].innerHTML

  
    var btn = document.getElementById("button-add")
    cid= selectedRow.cells[0].innerHTML;
    if(btn){
      btn.innerHTML = "Update";//assign the value call update
      btn.setAttribute("onclick", "update(cid)")
    }
  }

  function getData(){
    var formData = {
      courseid : document.getElementById("cid").value,
      coursename :document.getElementById("coursename").value,

    }
    return formData
  }

  

function update(cid){
    var newData = getData();

    fetch("/course/"+ cid,{
        method: "PUT",
        body: JSON.stringify(newData),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    }).then(res => {
        if (res.ok){
            selectedRow.cells[0].innerHTML = newData.courseid;
            selectedRow.cells[1].innerHTML = newData.coursename;

            var btn  = document.getElementById("button-add")
            if (btn){
                btn.innerHTML = "Add";
                btn.setAttribute("onclick", "addCourse()")
                selectedRow = null;

                resetform();
            } else{
                alert("Server: update request error;")
            }
}
})
}





//delete course
function deleteCourse(r){
    if(confirm('Are you sure you want to DELETE this?')){//shows the comfirmation message
      selectedRow= r.parentElement.parentElement;//assigns the parentelement row to selected row
      cid = selectedRow.cells[0].innerHTML;//extract the value of the first cell and assign to sid
  
      fetch('/course/'+cid, {//sends http delete request to the server which is responsible for delecting
        method:"DELETE",
        headers:{"Content-type":"application/json; charset=UTF-8"}
      });//we can add the promise method then here.
      var rowIndex = selectedRow.rowIndex;//get the index of the selectedrow, index starts from 0 cuz 
      if(rowIndex>0){//th is row 0
          document.getElementById("myTable").deleteRow(rowIndex)
      }
      selectedRow=null;
    }
  }
  