// window.onload=function(){
//     fetch('/students')
//         .then(response => response.text())//text f will extract from the body
//         .then(data => getStudents(data));
//     //get all courses
//     fetch('/courses')
//         .then(response=> response.text())
//         .then(data=> getCourses(data))


// }

// //getstudents

// function getStudents(data){
//     const studentIDs = []//to store all std id
//     const allStudents = JSON.parse(data)
//     //add student id to student list
//     allStudents.forEach(stud => {//iterate 
//         studentIDs.push(stud.stdid)//print. extract and add it to stdid 
//     });
//     // get the sid input field
//     var select = document.getElementById("sid")
// // iterate over the students and create a new option for each ID
//     for (var i = 0; i < studentIDs.length; i++) {
//         var sid = studentIDs[i];
//         var option = document.createElement("option")
//             option.textContent= sid;
//             option.value = sid;
//             select.appendChild(option);
//     }

// }

// //get course

// function getCourses(data){
//     const courseIDs=[]
//     const allCourses = JSON.parse(data)
//     allCourses.forEach(course=>{
//         courseIDs.push(course.courseid)
//     })
//     var option="";
//     for(var i=0; i<courseIDs.length; i++){
//         option+= '<option value="'+ courseIDs[i]+'">'+courseIDs[i]+"</option>"
//     }
//     document.getElementById("cid").innerHTML=option;
// }

// function addEnroll(){
//     var_data = {
//         stdid: parseInt(document.getElementById("sid").value),
//         cid: document.getElementById("cid").value,
//         date:""
//     }
//     var sid =_data.stdid;
//     var cid= _data.cid;

//     if (isNaN(sid)|| cid ==""){
//         alert("select valid data")
//         return
//     }
//     fetch('/enroll',{
//         method:"POST",
//         body: JSON.stringify(_data),
//         headers:{"Content-type":"appliction/json; charset=UFT-8"}
//     }).then(response => {
//         if (response.ok) {
//             fetch('/enroll/'+sid+'/'+cid)
//             .then(response => response.text())
//             .then(data => getEnrolled(data))
//         } else {
//             throw new Error(response.statusText)
//         }
//     }).catch(e => {
//         if (e == "Error: Forbidden") {
//             alert(e+". Duplicate entry!")
//         }
//     });
//     resetFields();
// }

// function getEnrolled(data) {
//     const enrolled = JSON.parse(data)
//     showTable(enrolled)
// }

// //helper function
// function showTable(enrolled) {
//     // Find a <table> element with id="myTable":
//     var table = document.getElementById("myTable");
//     // Create an empty <tr> element and add it to the last position of the table:
//     var row = table.insertRow(table.length);
//     // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
//     var td=[]
//      for(i=0; i<table.rows[0].cells.length; i++){
//         td[i] = row.insertCell(i);
//     }
//     td[0].innerHTML = enrolled.stdid;
//     td[1].innerHTML = enrolled.cid;
//     td[2].innerHTML = enrolled.date.split("T")[0]; // show only date,ignore time
//     td[3].innerHTML = '<input type="button" onclick="deleteEnroll(this)"value="Delete" id="button-1">';
// }

// function getAllEnroll(data) {
//     const allenroll = JSON.parse(data)
//     allenroll.forEach(enroll => {
//         showTable(enroll)
//     });
// }  
// fetch('/enrolls')
// .then(response => response.text())
// .then(data => getAllEnroll(data));
  

// const deleteEnroll = async(r) => {
//     if (confirm('Are you sure you want to DELETE this?')){
//         selectedRow = r.parentElement.parentElement;
//         sid = selectedRow.cells[0].innerHTML;
//         cid = selectedRow.cells[1].innerHTML;
//         fetch('/enroll/'+sid+"/"+cid, {
//             method: "DELETE",
//             headers: {"Content-type": "application/json; charset=UTF-8"}
//         }).then( response => {
//             if (response.ok) {
//                 var rowIndex = selectedRow.rowIndex; // index starts from 0
//                 if (rowIndex>0) { //th is row 0
//                     document.getElementById("myTable").deleteRow(rowIndex);
//                 }
//             }
//         });
//     }
// }
window.onload = function (){
    fetch('/students')
    .then(response => response.text())
    .then(data => getStudents(data));
    
    fetch('/courses')
    .then(response => response.text())
    .then(data => getCourses(data))

    fetch('/enrolls')
    .then(response => response.text())
    .then(data => getAllEnroll(data));

  
} 


function getStudents(data){
    const studentIDs = []//stores all student id in studentIDs
    const allStudents = JSON.parse(data)
    //add student id to students list
    //stud - variable to iterate 
    allStudents.forEach(stud => {
        studentIDs.push(stud.stdid)
    });
    //get the sid input field
    var select= document.getElementById("sid")
    //iterate over the students and create a new option for each
    for (var i = 0; i < studentIDs.length; i++) {
        var sid = studentIDs[i];
        var option = document.createElement("option")
        option.textContent = sid;
        option.value = sid;
        select.appendChild(option);
    }
}



function getCourses(data){
    const courseIDs = []
    const allCourses = JSON.parse(data)
    allCourses.forEach(course => {
        courseIDs.push(course.courseid)
    });
    var option = "";
    for (var i=0; i<courseIDs.length; i++){
        option += '<option value = "'+courseIDs[i] +'">' + courseIDs[i]+ "</option>"
    }
    document.getElementById("cid").innerHTML = option;
}

//add enrollment

function addEnroll() {
    var _data = {
        stdid: parseInt(document.getElementById("sid").value),
        courseid: document.getElementById("cid").value,
        date: ""
    };
    var sid = _data.stdid;
    var cid = _data.courseid;

    if (isNaN(sid) || cid === "") {
        alert("Select valid data");
        return;
    }

    fetch('/enroll', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (response.ok) {
            fetch('/enroll/'+sid+'/'+cid)
            .then(response => response.text())
            .then(data => getEnrolled(data))
        } else {
            throw new Error(response.statusText)
        }
    }).catch(e => {
        if (e == "Error: Forbidden") {
            alert(e+". Duplicate entry!")
        }
    });
    resetFields();

}
function getEnrolled(data) {
    const enrolled = JSON.parse(data)
    showTable(enrolled)
    }

//show table
function showTable(enrolled){
    //find a <table> element with id = "myTable"
    var table = document.getElementById("myTable");
//creating an empty <tr> element and add it to the last position of the table
    var row = table.insertRow(table.length);
//insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var td=[]
    for(i=0; i<table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i);
    }
    td[0].innerHTML = enrolled.stdid;
    td[1].innerHTML = enrolled.courseid;
    //show only date, ignore time
    td[2].innerHTML = enrolled.date.split("T")[0];
    td[3].innerHTML = '<input type="button" onclick="deleteEnroll(this)"value="Delete" id="button-1">';
    
}

//getAllEnrollment
function getAllEnroll(data) {
    const allenroll = JSON.parse(data)
    allenroll.forEach(enroll => {
    showTable(enroll)
});
}

//delete 
const deleteEnroll = async(r) => {
    if (confirm('Are you sure you want to DELETE this?')){
        selectedRow = r.parentElement.parentElement;
        sid = selectedRow.cells[0].innerHTML;
        cid = selectedRow.cells[1].innerHTML;
        fetch('/enroll/'+sid+"/"+ cid, {
            method: "DELETE",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }). then( response => {
            if (response.ok) {
                var rowIndex = selectedRow.rowIndex; // index starts from 0
                if (rowIndex>0) { //th is row 0
                    document.getElementById("myTable").deleteRow(rowIndex);
                }
            }
        });
    }
}