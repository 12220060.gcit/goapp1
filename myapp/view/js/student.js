window.onload = function(){
  fetch('/students')
  .then(response => response.text())
  .then(data => showStudents(data))
}


function addStudent() {
    var data = {
      stdid: parseInt(document.getElementById("sid").value),
      fname: document.getElementById("fname").value,
      lname: document.getElementById("lname").value,
      email: document.getElementById("email").value,
    };

    var sid = data.stdid;

    if (isNaN(sid)) {
      alert("Enter valid student ID")
      return
      } else if (data.email == "") {
      alert("Email cannot be empty")
      return
      }
       else if(data.fname == ""){
      alert("first name cannot be empty")
      return
      }
      
    fetch("/student", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-type": "application/json;charset-UTF-8" },
    }).then((response1) => {
      if (response1.ok) {
        fetch("/student/" + sid)
          .then((response2) => response2.text())
          .then((data) => showStudent(data));
      }else{
        throw new Error(response1.statusText)
      }
    }).catch(e =>{
      // if(e.message ==303){
      //   alert("user not logged in.")
      //   window.open("index.html", "_self")
      // }else if(e.message == 500){
      //   alert("server error!")
      // }
        alert(e)
    })
    // resetform();

  }
//to show one student
   function showStudent(data){
    const student = JSON.parse(data)
    student.forEach(stud =>{
      newRow(stud)
    });
    // // Find a <table> element with id="myTable":
    // var table = document.getElementById("myTable")
    // // Create an empty <tr> element and add to the last position of the table:
    // var row = table.insertRow(table.length)
    // // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
    // var td=[]
    // for (i =0;i<table.rows[0].cells.length;i++){
    //   td[i] = row.insertCell(i)
    // }
    // // Add student detail to the new cells:
    // td[0].innerHTML=student.stdid
    // td[1].innerHTML=student.fname
    // td[2].innerHTML=student.lname
    // td[3].innerHTML=student.email
    // td[4].innerHTML='<input type = "button" onclick = "deleteStudent(this)" value="delete" id ="button-1">'
    // td[5].innerHTML='<input type = "button" onclick = "updateStudent(this)" value="edit" id ="button-2">'
  
   }

//set form fields to empty
function resetform(){
  document.getElementById("sid").value = "";
  document.getElementById("fname").value = "";
  document.getElementById("lname").value = "";
  document.getElementById("email").value = "";

}

//function to show all students

function showStudents(data){
  const students = JSON.parse(data)
  students.forEach(stud =>{
    // Find a <table> element with id="myTable":
    var table = document.getElementById("myTable")
    // Create an empty <tr> element and add to the last position of the table:
    var row = table.insertRow(table.length)
    // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
    var td=[]
    for (i =0;i<table.rows[0].cells.length;i++){
      td[i] = row.insertCell(i)
    }
    // Add student detail to the new cells:
    td[0].innerHTML=stud.stdid
    td[1].innerHTML=stud.fname
    td[2].innerHTML=stud.lname
    td[3].innerHTML=stud.email
    td[4].innerHTML='<input type = "button" onclick = "deleteStudent(this)" value="delete" id ="button-1">'
    td[5].innerHTML='<input type = "button" onclick = "updateStudent(this)" value="edit" id ="button-2">'

  })
}

function newRow(student){//create the new row for the table we have in the table
  var table = document.getElementById("myTable")
  // Create an empty <tr> element and add to the last position of the table:
  var row = table.insertRow(table.length)
  // Insert new cells (<td> elements) at the 1st and 2nd position of the"new" <tr> element:
  var td=[]
  for (i =0;i<table.rows[0].cells.length;i++){
    td[i] = row.insertCell(i)
  }
  // Add student detail to the new cells:
  td[0].innerHTML=student.stdid
  td[1].innerHTML=student.fname
  td[2].innerHTML=student.lname
  td[3].innerHTML=student.email
  td[4].innerHTML='<input type = "button" onclick = "deleteStudent(this)" value="delete" id ="button-1">'
  td[5].innerHTML='<input type = "button" onclick = "updateStudent(this)" value="edit" id ="button-2">'//this point to the input type element

}

var selectedRow= null
function updateStudent(r){
  selectedRow = r.parentElement.parentElement;
  //fill in the form fields with selected row data
  document.getElementById("sid").value=selectedRow.cells[0].innerHTML//access the value store
  document.getElementById("fname").value=selectedRow.cells[1].innerHTML
  document.getElementById("lname").value=selectedRow.cells[2].innerHTML
  document.getElementById("email").value=selectedRow.cells[3].innerHTML

  var btn = document.getElementById("button-add")
  sid= selectedRow.cells[0].innerHTML;
  if(btn){
    btn.innerHTML = "Update";//assign the value call update
    btn.setAttribute("onclick", "update(sid)")
  }
}
function getFormData(){
  var formData = {
    stdid : parseInt(document.getElementById("sid").value),
    fname :document.getElementById("fname").value,
    lname :document.getElementById("lname").value,
    email :document.getElementById("email").value,
  }
  return formData
}

function update(sid){//data to be sent to update request
  var newData = getFormData()
  fetch('/student/'+sid, {
    method: "PUT",
    body: JSON.stringify(newData),
    headers: {"Content-type": "application/json; charset=UTF-8"}
  }).then (res => {
  if (res.ok) {
  // fill in selected row with updated value
    selectedRow.cells[0].innerHTML = newData.stdid;
    selectedRow.cells[1].innerHTML = newData.fname;
    selectedRow.cells[2].innerHTML = newData.lname;
    selectedRow.cells[3].innerHTML = newData.email;
  // set to previous value
  var button = document.getElementById("button-add");
  if (btn){
    btn.innerHTML="Add"
    btn.setAttribute("onclick", "addStudent()")
    selectedRow = null;

    resetform();
  } else{
    alert("server: update request error")
  }
}

})
}


//delete student
function deleteStudent(r){
  if(confirm('Are you sure you want to DELETE this?')){//shows the comfirmation message
    selectedRow= r.parentElement.parentElement;//assigns the parentelement row to selected row
    sid = selectedRow.cells[0].innerHTML;//extract the value of the first cell and assign to sid

    fetch('/student/'+sid, {//sends http delete request to the server which is responsible for delecting
      method:"DELETE",
      headers:{"Content-type":"application/json; charset=UTF-8"}
    });//we can add the promise method then here.
    var rowIndex = selectedRow.rowIndex;//get the index of the selectedrow, index starts from 0 cuz 
    if(rowIndex>0){//th is row 0
        document.getElementById("myTable").deleteRow(rowIndex)
    }
    selectedRow=null;
  }
}
