package routes

import (
	"log"
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitialisedRoutes() {
	// gorilla mux router creating
	router := mux.NewRouter()
	// registering router and mapping the handler function
	router.HandleFunc("/home", controller.HomeHandler) // didnt cal func to call automatically arguemnt
	// "/home" is the ruote
	// new router
	// router.HandleFunc("/urlParameter/{myname}", ParameterHandler)
	// start the  http helper

	// student
	//add
	router.HandleFunc("/student", controller.AddStudent).Methods("POST") //to post
	//read
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET") //to get the data from url
	//update
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT") //to update the data
	//delete
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE") //to delete the data

	//GET ALL THE STUDENTS
	router.HandleFunc("/students", controller.GetAllStuds).Methods("GET") //print all students

	//admin
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")

	//--------------------------------------------------------------------------------------------
	// course
	// add
	router.HandleFunc("/course", controller.AddCourse).Methods("POST") //to post course
	//read
	router.HandleFunc("/course/{cid}", controller.GetCour).Methods("GET") //to get the data from url
	//update
	router.HandleFunc("/course/{cid}", controller.UpdateCour).Methods("PUT") //to update the data
	//delete
	router.HandleFunc("/course/{cid}", controller.DeleteCour).Methods("DELETE") //to delete the data
	//GET ALL THE courses
	router.HandleFunc("/courses", controller.GetAllCours).Methods("GET") //print all students

	router.HandleFunc("/logout", controller.Logout) //logout

	//enroll
	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")                  //
	router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")    //
	router.HandleFunc("/enrolls", controller.GetEnrolls).Methods("GET")              //
	router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("GET") //

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
	log.Println("Application running on port 8080....") //just to print somemess

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		os.Exit(1)
	}

}
