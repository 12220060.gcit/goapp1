package model

import postgres "myapp/datastore/postgress"

type Enroll struct {
	StdId         int64  `json:"stdid"`
	CourseID      string `json:"courseid"`
	Date_Enrolled string `json:"date"`
}

const ( //insert, update, delete use returnning
	queryEnroll = "INSERT INTO enroll(std_id, course_id, date_enrolled) VALUES($1, $2, $3) RETURNING stdid;"
)

func (e *Enroll) EnrollStud() error {
	row := postgres.Db.QueryRow(queryEnroll, e.StdId, e.CourseID, e.Date_Enrolled)
	err := row.Scan(&e.StdId)
	return err

}

const queryGetEnroll = "SELECT std_id, course_id, date_enrolled FROM enroll WHERE std_id=$1 and course_id=$2;"

func (e *Enroll) Get() error {
	return postgres.Db.QueryRow(queryGetEnroll,
		e.StdId, e.CourseID).Scan(&e.StdId, &e.CourseID, &e.Date_Enrolled)
}

// get all enrollment
func GetAllEnrolls() ([]Enroll, error) {
	rows, getErr := postgres.Db.Query("SELECT std_id, course_id, date_enrolled from enroll;")
	if getErr != nil {
		return nil, getErr
	}
	//create a slice of type course
	enrolls := []Enroll{}

	for rows.Next() {
		var e Enroll
		dbErr := rows.Scan(&e.StdId, &e.CourseID, &e.Date_Enrolled)
		if dbErr != nil {
			return nil, dbErr
		}
		enrolls = append(enrolls, e)
	}
	rows.Close()
	return enrolls, nil
}

// delete
const queryDeleteEnroll = "DELETE FROM enroll WHERE std_id=$1 and course_id=$2;"

func (e *Enroll) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteEnroll, e.StdId, e.CourseID); err != nil {
		return err
	}
	return nil
}
