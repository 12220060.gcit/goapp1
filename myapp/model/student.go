package model

import (
	postgres "myapp/datastore/postgress"
)

type Student struct {
	StdID     int64  `json:"stdid"`
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
}

// student
const (
	queryInsert     = "INSERT INTO student(stdid,firstname,lastname,email) VALUES($1, $2, $3, $4);"
	queryGetUser    = "SELECT stdid, firstname, lastname, email FROM student WHERE stdid=$1"
	userUpdate      = "UPDATE student SET stdid=$1, firstname=$2, lastname=$3, email=$4 WHERE stdid=$5 RETURNING stdid" //
	queryDeleteUser = "DELETE FROM student WHERE stdid=$1;"
)

// create
func (s *Student) Create() error {
	_, err := postgres.Db.Exec(queryInsert, s.StdID, s.FirstName, s.LastName, s.Email)
	return err
	// fmt.Println(s)
}

// read//get
func (s *Student) Read() error {
	return postgres.Db.QueryRow(queryGetUser,
		s.StdID).Scan(&s.StdID, &s.FirstName, &s.LastName, &s.Email)
}

// or we can do it like this too
// func (s *Student) Read() error {
//      postgres.Db.QueryRow(queryGetUser, s.StdID)
// 	err := row.Scan(&s.StdID, &s.FirstName, &s.LastName, &s.Email)
// 	return err
// }

// update//put
func (s *Student) Update(oldID int64) error {
	err := postgres.Db.QueryRow(userUpdate, s.StdID, s.FirstName, s.LastName, s.Email, oldID).Scan(&s.StdID)
	return err
}

// delete
func (s *Student) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteUser, s.StdID); err != nil {
		return err
	}
	return nil
}

//or
// func (s *Student) Delete() error {
// 	if _, err := postgres.Db.QueryRow(queryDeleteUser, s.StdID); err != nil {
// 		return err
// 	}
// 	return nil
// }

// get all students
func GetAllStuds() ([]Student, error) {
	rows, err := postgres.Db.Query("SELECT * FROM student;")
	if err != nil {
		return nil, err
	}
	//create a slice of type student
	students := []Student{}
	for rows.Next() { // iterates rows one by one
		var s Student
		dbErr := rows.Scan(&s.StdID, &s.FirstName, &s.LastName, &s.Email) //read
		if dbErr != nil {
			return nil, dbErr
		}
		students = append(students, s)
	}
	rows.Close()
	return students, nil
}

// course
type Course struct {
	CourseID   string `json:"courseid"`
	CourseName string `json:"coursename"`
}

const (
	courseInsert      = "INSERT INTO course(courseid, coursename) VALUES($1, $2);"
	qGetCourse        = "SELECT courseid, coursename FROM course WHERE courseid=$1"
	courseUpdate      = "UPDATE course SET courseid=$1, coursename=$2 WHERE courseid=$3 RETURNING courseid" //
	queryDeleteCourse = "DELETE FROM course WHERE courseid=$1;"
)

// create
func (c *Course) Create() error {
	_, err := postgres.Db.Exec(courseInsert, c.CourseID, c.CourseName)
	return err
	// fmt.Println(s)
}

// read//get
func (c *Course) Read() error {
	return postgres.Db.QueryRow(qGetCourse,
		c.CourseID).Scan(&c.CourseID, &c.CourseName)
}

// update//put
func (c *Course) Update(oldID string) error {
	err := postgres.Db.QueryRow(courseUpdate, c.CourseID, c.CourseName, oldID).Scan(&c.CourseID)
	return err
}

// delete
func (c *Course) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteCourse, c.CourseID); err != nil {
		return err
	}
	return nil
}

// get all course
func GetAllCours() ([]Course, error) {
	rows, err := postgres.Db.Query("SELECT * FROM course;")
	if err != nil {
		return nil, err
	}
	//create a slice of type student
	courses := []Course{}
	for rows.Next() { // iterates rows one by one
		var c Course
		dbErr := rows.Scan(&c.CourseID, &c.CourseName) //read
		if dbErr != nil {
			return nil, dbErr
		}
		courses = append(courses, c)
	}
	rows.Close()
	return courses, nil
}
